const fetchJSON = async (url) => {
    let resp = await fetch(url);
    return await resp.json();
};

const updateTODOs = async () => {
    let todos = await fetchJSON("/api/todos");
    let list = document.getElementById("tasks");
    list.innerHTML = "";
    todos.forEach(task => {
        let bullet = document.createElement("li");
        bullet.className = "task";
        let desc = document.createElement("div");
        desc.textContent = task.description;
        desc.className = "description";
        bullet.appendChild(desc);
        let insertD = document.createElement("div");
        insertD.className = "insertDate";
        insertD.textContent = task.insertDate;
        bullet.appendChild(insertD);
        let done = document.createElement("input");
        done.type = "checkbox";
        done.checked = task.done;
        done.className = "t-done";
        done.addEventListener("change", () => {
            updateTODO(task.id, task.description, done.checked);
        });
        bullet.appendChild(done);
        let del = document.createElement("button");
        del.className = "del-btn";
        del.addEventListener("click", () => {
            deleteTODO(task.id);
        });
        del.textContent = "❌";
        bullet.appendChild(del);
        list.appendChild(bullet);
    });
};

const deleteTODO = async (id) => {
    let resp = await fetch(`/api/todos/${id}`, {
        method: "DELETE"
    });
    if (resp.status !== 204) {
        alert("Si è verificato un errore durante l'eliminazione");
    }
    updateTODOs();
};

const updateTODO = async (id, desc, done) => {
    let resp = await fetch(`/api/todos/${id}`, {
        method: "PUT",
        body: JSON.stringify({
            description: desc,
            done: done
        }),
        headers: {
            "Content-Type": "application/json"
        }
    });
    if (resp.status !== 200) {
        alert("Si è verificato un errore durante l'aggiornamento");
    }
    updateTODOs();
};

const insertTODO = async (desc) => {
    let resp = await fetch(`/api/todos`, {
        method: "POST",
        body: JSON.stringify({
            description: desc
        }),
        headers: {
            "Content-Type": "application/json"
        }
    });
    if (resp.status !== 201) {
        alert("Si è verificato un errore durante l'inserimento");
    }
    updateTODOs();
};

window.addEventListener("DOMContentLoaded", () => {
    updateTODOs();
    document.getElementById("add-t-f").addEventListener("submit", async (event) => {
        event.preventDefault();
        let description = document.getElementById("task_description");
        await insertTODO(description.value);
        description.value = "";
    });
});