<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TodoController extends Controller
{
    public function add(Request $request)
    {
        $description = $request->input('description');
        app('db')->insert(
            "INSERT INTO tasks(description, done, insertDate) values('$description', 0, CURRENT_TIMESTAMP())"
        );
        return new Response(null, 201);
    }

    public function update(Request $request,$id) {
        $description = $request->input('description');
        $done = $request->input('done');
        if ($done) {
            $done_val = 1;
        } else {
            $done_val = 0;
        }
        app('db')->insert(
            "UPDATE tasks SET description = '$description', done = '$done_val' WHERE id = $id"
        );

        return new Response($description, 200);
    }

    public function delete(Request $request, $id)
    {   
        app('db')->insert(
            "DELETE FROM tasks WHERE id = '$id'"
        );

        return new Response(null, 204);
    }

    public function select(Request $request, $id)
    {   
        $result = app('db')->select(
            "SELECT * FROM taks WHERE id = $id"
        );

        return $result;
    }
}
